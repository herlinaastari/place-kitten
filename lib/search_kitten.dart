import 'package:flutter/material.dart';

class SearchKitten extends StatefulWidget {
  const SearchKitten({super.key});

  @override
  _SearchKittenState createState() => _SearchKittenState();
}

class _SearchKittenState extends State<SearchKitten> {
  final _formKey = GlobalKey<FormState>();
  final widthController = TextEditingController();
  final heightController = TextEditingController();

  double imageWidth = 100.0;
  double imageHeight = 100.0;
  bool isGrayscaled = false;
  String imageUrl = '';

  @override
  Widget build(BuildContext context) {
    setState(() {});
    return Scaffold(
        appBar: AppBar(
          title: const Text('Search'),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        controller: widthController,
                        decoration: const InputDecoration(
                            labelText: 'Width',
                            hintText: 'Width of the image',
                            contentPadding: EdgeInsets.all(16),
                            focusColor: Colors.red,
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.teal)
                            )
                        ),
                        textAlign: TextAlign.left,
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          return (value == null || value.isEmpty)
                              ? 'Please enter the width'
                              : null;
                        },
                      ),
                      TextFormField(
                        controller: heightController,
                        decoration: const InputDecoration(
                            labelText: "Height",
                            hintText: 'Height of the image',
                            contentPadding: EdgeInsets.all(16),
                            focusColor: Colors.teal,
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.teal)
                            )
                        ),
                        textAlign: TextAlign.left,
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          return (value == null || value.isEmpty)
                              ? 'Please enter the width'
                              : null;
                        },
                      ),
                    ],
                  )),
              CheckboxListTile(
                title: const Text("Grayscale?"),
                value: isGrayscaled,
                onChanged: (newValue) {
                  setState(() {
                    isGrayscaled = newValue ?? false;
                  });
                },
                activeColor: Colors.teal,
                controlAffinity: ListTileControlAffinity.trailing,
              ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      searchButtonDidClick();
                    });
                  },
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.teal)
                  ),
                  child: const Text("Search")),
              if (imageUrl.isNotEmpty) ...[
                const Padding(padding: EdgeInsets.all(16)),
                Image.network(
                  imageUrl,
                  width: imageWidth,
                  height: imageHeight,
                ),
              ]
            ],
          ),
        ));
  }

  void searchButtonDidClick() {
    if (_formKey.currentState!.validate()) {
      var additional = isGrayscaled == true ? '/g' : '';
      var width = '/' + widthController.text;
      var height = '/' + heightController.text;

      imageWidth = double.parse(widthController.text);
      imageHeight = double.parse(heightController.text);
      var url = 'http://placekitten.com' + additional + width + height;
      imageUrl = url;
    }
  }

  Future<void> showErrorDialog() async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Error'),
            content: SingleChildScrollView(
              child: ListBody(
                children: const [
                  Text('This is a demo alert dialog.'),
                ],
              ),
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Ok'))
            ],
          );
        });
  }
}
