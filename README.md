# Place Kitten

I created this Flutter project for learning purpose.
The API is from https://placekitten.com/ . It is an open API and it is not mine, it belongs to the respectful owner.

The project is to find an image of kitten based on the input (image width, image size, and grayscaled or not).

![Demo](https://gitlab.com/herlinaastari/place-kitten/-/raw/master/demo/Demo.gif)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
